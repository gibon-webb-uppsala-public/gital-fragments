<?php
/**
 * Plugin Name: Gital Fragments
 * Author: Gibon Webb Uppsala
 * Version: 1.4.0
 *
 * Author URI: https://gibon.se/
 * Description: The Gital Fragments is made with passion in Uppsala, Sweden. If you'd like support, please contact us at webb.uppsala@gibon.se.
 *
 * @package Gital Fragments
 */

namespace gital_fragments;

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * Load textdomain and languages
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 */
function textdomain() {
	load_plugin_textdomain( 'gital-fragments', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'gital_fragments\textdomain' );

// Constants.
if ( ! defined( 'G_FRA_ROOT' ) ) {
	define( 'G_FRA_ROOT', plugins_url( '', __FILE__ ) );
}
if ( ! defined( 'G_FRA_ASSETS' ) ) {
	define( 'G_FRA_ASSETS', G_FRA_ROOT . '/assets' );
}
if ( ! defined( 'G_FRA_ROOT_PATH' ) ) {
	define( 'G_FRA_ROOT_PATH', plugin_dir_path( __FILE__ ) );
}
if ( ! defined( 'G_FRA_VENDOR_PATH' ) ) {
	define( 'G_FRA_VENDOR_PATH', G_FRA_ROOT_PATH . 'vendor/' );
}
if ( ! defined( 'G_FRA_ASSETS_PATH' ) ) {
	define( 'G_FRA_ASSETS_PATH', G_FRA_ROOT_PATH . 'assets/' );
}
if ( ! defined( 'G_FRA_CLASSES_PATH' ) ) {
	define( 'G_FRA_CLASSES_PATH', G_FRA_ROOT_PATH . 'classes/' );
}

// Autoloader.
require G_FRA_VENDOR_PATH . 'autoload.php';

// Init the updater.
use YahnisElsts\PluginUpdateChecker\v5\PucFactory;
$myUpdateChecker = PucFactory::buildUpdateChecker(
	'https://packages.gital.se/wordpress/gital-fragments.json',
	__FILE__,
	'gital-fragments'
);

// Classes.
Fragments::get_instance();

/**
 * Enqueue public scripts
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 */
function assets() {
	wp_register_style( 'g_fragments_style', G_FRA_ASSETS . '/styles/gital.fragments.min.css', array(), '1.0.0' );
	wp_enqueue_style( 'g_fragments_style' );

	wp_register_script( 'g_fragments_script', G_FRA_ASSETS . '/scripts/gital.fragments.min.js', array(), '1.1.0', true );
	wp_enqueue_script( 'g_fragments_script' );
}
add_action( 'wp_enqueue_scripts', 'gital_fragments\assets' );
