=== Gital Fragments ===
Contributors: gibonadmin
Requires at least: 5.0
Tested up to: 6.4
Requires PHP: 7.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The Gital Fragments is made with passion in Uppsala, Sweden. If you'd like support, please contact us at webb.uppsala@gibon.se.

== Description ==
The Gital Fragments is made with passion in Uppsala, Sweden. If you'd like support, please contact us at webb.uppsala@gibon.se.

== How to use ==

Create the callback function and hook into the g_fragment filter.

function my_callback( $args ) {
	if ( 'my_fragment_callback' === $args[0] ) {
		return '<h1>' . get_the_title( $args[1][0] ) . '</h1>';
	}
}
add_filter( 'g_fragment', 'my_callback' );

Then, in the frontend, add the class g-fragment and the attribute g-fragment-data. g-fragment-data should be an JSON array with the callback and the arguments.

<div class="my-fragment g-fragment g-fragment--placeholder" g-fragment-data='<?php echo wp_json_encode( array( 'my_fragment_callback', array( get_the_id() ) ) ); ?>'></div>

== Changelog ==

= 1.4.0 - 2024.09.04 =
* The plugin now utilizes the new repo.

= 1.3.2 - 2024.02.29 = 
* Update: Instead of requesting one fragment at a time. It's now requesting everything at once.

= 1.2.1 - 2024.02.16 = 
* Update: Made the init function available from frontend via g_fragments_load().

= 1.0.3 - 2024.02.13 = 
* Update: Init.