<?php
/**
 * Fragments class
 *
 * @package Gital Library
 */

namespace gital_fragments;

if ( ! class_exists( 'Fragments' ) ) {
	/**
	 * Fragments
	 *
	 * Fragments description
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 * @since 1.0.0
	 */
	class Fragments extends Singleton {
		public function init() {
			add_action( 'rest_api_init', array( $this, 'register_rest_route' ) );
		}

		/**
		 * Register a new route for the REST API
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function register_rest_route() {
			register_rest_route(
				'gital',
				'/fragments/get_fragments',
				array(
					'methods'             => 'POST',
					'callback'            => array( $this, 'get_fragments_callback' ),
					'permission_callback' => '__return_true',
				)
			);
		}

		/**
		 * Get fragment callback
		 *
		 * @param array $params The fragment arguments.

		 * @return string The html to replace the fragment with.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function get_fragments_callback( $params ) {
			$parameters = $params->get_params();
			$fragments  = $parameters['payload'] ?? array();

			foreach ( $fragments as $index => $fragment ) {
				$fragments[ $index ] = apply_filters( 'g_fragment', $fragment );
			}

			return $fragments;
		}
	}
}
