/*
 * WEBPACK CONFIG
 *
 * Author : Gustav Gesar
 * Version: 3.0.0
 *
 */

// Plugins
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const RemoveEmptyScriptsPlugin = require('webpack-remove-empty-scripts');
const path = require('path');
require('dotenv').config();

// Paths
const paths = {
	styles: path.resolve('/assets/styles'),
	scripts: path.resolve('/assets/scripts'),
	node_modules: path.resolve('/node_modules'),
};

// Config
module.exports = (env, argv) => {
	const config = {
		entry: {
			// Gital Fragments
			[`${paths.styles}/gital.fragments`]: [
				`${paths.styles}/gital.fragments.scss`,
			],
			[`${paths.scripts}/gital.fragments`]: [
				`${paths.scripts}/gital.fragments.ts`,
			],
		},
		output: {
			path: path.resolve( __dirname ),
			filename: '[name].min.js',
			sourceMapFilename: '[file].map',
		},
		module: {
			rules: [
				{
					// JS
					test: /\.js$/,
					use: {
						loader: 'babel-loader',
						options: {
							presets: [ '@babel/preset-env' ],
						},
					},
				},
				{
					// TS
					test: /\.ts$/,
					use: {
						loader: 'ts-loader',
						options: {
							onlyCompileBundledFiles: true,
						},
					},
					exclude: /node_modules/,
				},
				{
					// CSS
					test: /\.css$/,

					// Loaders are applying from bottom to top
					use: [
						{
							// Get all transformed CSS and extracts it into separate single bundled file
							loader: MiniCssExtractPlugin.loader,
						},
						{
							// Resolves url() and @imports inside CSS
							loader: 'css-loader',
							options: {
								sourceMap: true,
								url: false,
							},
						},
					],
				},
				{
					// Sass
					test: /\.(sa|sc)ss$/,

					// Loaders are applying from bottom to top
					use: [
						{
							// Get all transformed CSS and extracts it into separate single bundled file
							loader: MiniCssExtractPlugin.loader,
						},
						{
							// Resolves url() and @imports inside CSS
							loader: 'css-loader',
							options: {
								sourceMap: true,
								url: false,
							},
						},
						{
							// Apply postCSS fixes like autoprefixer
							loader: 'postcss-loader',
							options: {
								sourceMap: true,
								postcssOptions: {
									plugins: [
										require( 'autoprefixer' )( {
											grid: false,
											flexbox: false,
										} ),
									],
								},
							},
						},
						{
							// Complie SASS to standard CSS
							loader: 'sass-loader',
							options: {
								implementation: require( 'sass' ),
								sourceMap: true,
							},
						},
					],
				},
				{
					// SVG
					test: /\.(svg)$/,
					type: 'asset/inline',
				},
			],
		},
		plugins: [
			new RemoveEmptyScriptsPlugin(),
			new MiniCssExtractPlugin( {
				filename: '[name].min.css',
			} ),
			new BrowserSyncPlugin( {
				host: 'localhost',
				port: 3000,
				proxy: process.env.WP_HOME,
			} ),
		],
		optimization: {
			removeAvailableModules: false,
			removeEmptyChunks: false,
			splitChunks: false,
		},
		resolve: {
			symlinks: false,
		},
		stats: 'minimal',
	};

	if (argv.mode === 'development') {
		config.devtool = 'source-map';
	}
	return config;
};
