declare global {
	interface Window {
		g_fragments_load: any;
	}
}

type Fragment_Data = [string, any[]];
type Payload = Fragment_Data[];

/**
 * Replaces the contents of a DOM element with the provided HTML string.
 * 
 * @param {Element} fragment - The DOM element to be updated.
 * @param {string} content - The HTML content to replace the DOM element's contents.
 */
const render_fragments = (fragments: NodeListOf<Element>, resolved_fragments: string[]) => {
	fragments.forEach((fragment, index) => {
		fragment.outerHTML = resolved_fragments[index];
	});
};

/**
 * Retrieves a fragment from the server using asynchronous fetch requests with retries.
 * 
 * @param {Array<string, any[]>} fragment_data - A tuple containing the endpoint and additional data for fetching the fragment.
 * @returns {Promise<any>} - A promise that resolves with the fetched fragment content.
 * @throws {Error} - Throws an error if maximum retry count is exceeded.
 */
const get_fragments = async (payload: Payload) => {
	let retries = 3;
	while (retries > 0) {
		try {
			const response = await fetch('/wp-json/gital/fragments/get_fragments', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					payload,
				}),
			});

			if (response.ok) {
				return await response.json();
			}

			throw new Error(`HTTP error! Status: ${response.status}`);
		} catch (error) {
			console.error('Error fetching fragment:', error);
			await new Promise(resolve => setTimeout(resolve, 1000));
			retries -= 1;
		}
	}

	throw new Error('Maximum retries exceeded');
};

/**
 * Initializes the fragment retrieval and rendering process when the window has finished loading.
 */
export const init = () => {
	window.removeEventListener('load', init);
	const fragments = document.querySelectorAll('.g-fragment');

	if (fragments && fragments.length > 0) {
		const payload: Payload = Array.from(fragments).map(fragment => {
			const fragment_data = fragment.getAttribute('g-fragment-data');
			if (fragment_data) {
				return JSON.parse(fragment_data);
			};
		});

		get_fragments(payload)
			.then((response) => {
				render_fragments(fragments, response);
			})
			.catch((error) => {
				console.error('Error:', error);
			});
	}
};

window.addEventListener('load', init);
window.g_fragments_load = init;