# Gital Plugin Display Ads installation instructions

1. Rename the plugin folder (gital-fragments) to the name of the plugin.
2. Rename the root plugin file (gital-fragments.php) to the name of the plugin.
3. Rename the style and the script file to the name of the plugin.
4. Search and Replace (make sure to make a plan first and use "match case).

    Keywords not to forget:
    a. g_fragments
    b. fragments

    Do not miss the following files:
    a. The root plugin file
    b. The webpack config file
    c. The composer config file.

5. Run composer install and npm install.
6. Run composer setup.
7. Test npm run production and make sure you get minified versions of the styles and the scripts.
8. Remove the .git folder and reinit your projects repository if not handled by parent project.
9. Remove this file.

If the plugin should be updateable, run this routine:

1. Add "!vendor/yahnis-elsts" to .gitignore.
2. Require the composer package yahnis-elsts/plugin-update-checker.
3. Create a new project at gitlab under gibon-webb-uppsala-public, make it public.
4. Add the update snippet to the root php file. Copy from gital-library.
5. Replace the ROOT_PATH-constant and the gitlab-slug.
6. Add the composer_update.sh and replace the project ID.
7. Update composer.json, the root php file and readme.txt with the correct version.
8. Make sure composer_update.sh is not included in the git-repo.
9. Run gitupgrade.
